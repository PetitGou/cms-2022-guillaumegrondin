<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'n{kLpI/t;ljc  $Z)P^VcR|1BBquTAKP9842sAy; vyMc;zkYC-4TFfu+e~kuM=_' );
define( 'SECURE_AUTH_KEY',  'vWQr%F-7k!931u;/$Q677iAF}BTN|V%FwtGdA$$_&[~m;[&oBJm[y:mCZ}= }9*?' );
define( 'LOGGED_IN_KEY',    'B]`;>d!2KA<9y2bAg LyDP]E2B$8/m5f2`>;RI,P+t0}7{QGE$:8A1]m2[4@ZK2p' );
define( 'NONCE_KEY',        '9aN6?e`-vnOehV?YS`bO1Yp)px<rjq[Yjq#{_MS|)_+d(,?9@>C|D.t=pt>mWRqy' );
define( 'AUTH_SALT',        'wFK3)pu b@(UA{/0Y6P/i%gn@x%`7crMqL-&9;@T/_TA(h;$Jn/8Cv~H@:Z12-C*' );
define( 'SECURE_AUTH_SALT', ':YCZr&0g~}1ux)~#O^:fcN;Zre|eI_4llpp?c KWN&CzM8-=@oSND&9`.BS4San~' );
define( 'LOGGED_IN_SALT',   '`i5i}#cYM}_`(S%A.1w=|g&DU.1T!]Iyu9Y[rk>1A72WPH+$><SGt&~GL[>O@Uka' );
define( 'NONCE_SALT',       ',z`.F`Xr!:lqbQNq/}Qslu tEbA7^yG^JD98J2MC>zyTS0cTdpIBjK9`[6>J:aOy' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
